moon = imread('moon.jpg');
h = fspecial('laplacian',1);
h_moon = imfilter(moon,h);
sharp_moon = moon - h_moon;
figure(1), imshow(moon);
figure(2), imshow(sharp_moon);