function [ F ] = fft_vis(I)

    % Input:
    % I: the input image
    % Output:
    % F: 2D Fourier transform in a form amenable to visualization

    % Please follow the instructions in the comments to fill in the missing commands

    % 1) Apply Fourier transform to the image (MATLAB command fft2)
    fftI = fft2(I);

    % 2) Shift the spectrum (MATLAB command fftshift) so that the DC-component lies in the middle of the image
    fftI_shift = fftshift(fftI);
    % 3) Take the absolute value so that real and imaginary parts are superimposed
    abs_fftI_shift = abs(fftI_shift);

    % 4) Add 1e-1 and take the log (for visualization) it is useful to add 1e-1(10^-1)  to the abs and take the log, to see the spectrum in log-intensity
    F = log((abs_fftI_shift + (10^-1)));

end