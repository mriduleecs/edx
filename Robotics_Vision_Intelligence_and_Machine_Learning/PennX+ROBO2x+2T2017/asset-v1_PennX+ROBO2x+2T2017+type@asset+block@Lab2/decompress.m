function [Id] = decompress(Fcomp)

    % Input:
    % F: the compressed version of the image
    % Output:
    % Id: the approximated image

    % Please follow the instructions in the comments to fill in the missing commands.    
    
    % 1) Apply the inverse FFT shift (MATLAB command ifftshift)
    fftI_ishift = ifftshift(Fcomp);

    % 2) Compute the inverse FFT (MATLAB command ifft2)
    ifft_fftI_ishift = ifft2(fftI_ishift);
    % 3) Keep the real part of the previous output
    Id = real(ifft_fftI_ishift);

end
