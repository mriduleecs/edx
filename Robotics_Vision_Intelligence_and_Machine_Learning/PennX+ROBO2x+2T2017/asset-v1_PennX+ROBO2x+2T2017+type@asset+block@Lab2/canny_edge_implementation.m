clear;
image = (rgb2gray(imread('crank.jpg')));

threshold_low = 0.035;
threshold_high = 0.175;

% Gaussian Filter Definition from canny edge detection wikipedia.
G = [2,4,5,4,2;
    4,9,12,9,4;
    5,12,15,12,5;
    4,9,12,9,4;
    2,4,5,4,2];
G = 1/159 .* G;

% Filter the horizontal and vertical direction 

dx = [1,-1];
dy = [1;
    -1];

% Convolving G with its derivatives
Gx = conv2(G, dx, 'same');
Gy = conv2(G, dy, 'same');

% Convolving convolved G funtion with the Image

Ix = conv2(image, Gx, 'same');
Iy = conv2(image, Gy, 'same');

% Deriving the edge angle

mag =  (sqrt((Iy.^2) + (Ix.^2)));
angle = atan2(Iy,Ix);

% Non maximum Saperation 
imshow(image);
