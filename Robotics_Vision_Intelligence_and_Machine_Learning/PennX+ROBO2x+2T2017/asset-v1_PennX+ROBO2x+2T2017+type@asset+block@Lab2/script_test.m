I = imread('cameraman.tif');
I = im2double(imresize(I,[200 200]));
[rows,cols] = size(I);
n_0 = rows*cols;
M = n_0/8;
Fcomp = compress(I,sqrt(M));
Id = decompress(Fcomp);

figure(2);
subplot(1,2,1);
imshow(I);
subplot(1,2,2);
imshow(Id);

I = imread('cameraman.tif');
I = im2double(imresize(I,[200 200]));
Id = I + 0.1*rand(200);
SNR = compute_snr(I,Id)

