%% Create first order derivatives by following steps
%% Step 1 : create a gaussian kernel of size 5
%% Step 2 : Sigma = 1;
%%          gauss_filter = 1/(sqrt(2*pi)*sigma)*exp(-x.^2/(2*sigma));
%% Step 3 : smooth_x = Convolve input imae with the gaussian filter;
%% Step 4 : smooth = Convolve smooth_x with the transpose of gauss_filter;
%% Step 5 : Compute image gradients in x and y direction by convolving it with the dx and dy filter using conv2 function.;
%% Step 6 : Compute images I_xx, I_yy, I_xy at pixel (u,v) as :
%%          I_xx = I_x.^2; I_yy = I_y.^2; I_xy = I_x.*I_y; 
%% Step 7 : Filter I_xx, I_yy, I_xy with a gausssian filter;
%% Step 8 : Compute Corner Response at each pixel (u,v) as :
%%          R(u,v) = ((I_xx(u,v) * I_yy(u,v)) - I_xy(u,v)^2) - k(I_xx(u,v)+ I_yy(u,v))^2
%%          with k  = 0.06


img = imread('peppers.png');
img_gray = im2double(rgb2gray(img));

img_gray_smooth = gauss_blur(img_gray);
[I_x,I_y] = grad2d(img_gray_smooth);

I_xx = gauss_blur(I_x.^2);
I_yy = gauss_blur(I_y.^2);
I_xy = gauss_blur(I_x.*I_y);
	
k = 0.06;
%% Use the corner score equation from the lecture. 
R = ((I_xx.*I_yy) - I_xy.^2) - k*(I_xx + I_yy).^2;

function [I_x,I_y] = grad2d(img)
	%% compute image gradients in the x direction
	%% convolve the image with the derivative filter from the lecture
	%% using the conv2 function and the 'same' option
	dx_filter = [0.5 0 -0.5];
	I_x = conv2(img, dx_filter, 'same');

	%% compute image gradients in the y direction
	%% convolve the image with the derivative filter from the lecture
	%% using the conv2 function and the 'same' option
	dy_filter = [0.5 ; 0 ; -0.5];
	I_y = conv2(img, dy_filter, 'same');
end

function smooth = gauss_blur(img)
    %% Since the Gaussian filter is separable in x and y we can perform Gaussian smoothing by
    %% convolving the input image with a 1D Gaussian filter in the x direction then  
    %% convolving the output of this operation with the same 1D Gaussian filter in the y direction.

    %% Gaussian filter of size 5
    %% the Gaussian function is defined f(x) = 1/(sqrt(2*pi)*sigma)*exp(-x.^2/(2*sigma))
    x =  [-2 : 1 : 2];
    sigma = 1;
    gauss_filter = 1/(sqrt(2*pi)*sigma)*exp(-x.^2/(2*sigma));

    %% using the conv2 function and the 'same' option
    %% convolve the input image with the Gaussian filter in the x
    smooth_x = conv2(img, gauss_filter, 'same');
    %% convolve smooth_x with the transpose of the Gaussian filter
    smooth = conv2(smooth_x, transpose(gauss_filter), 'same');
end
